# coding: utf-8

import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OrdinalEncoder


COLUMNS = ["status", "duration", "credit_hist", "purpose",
           "credit_amount", "savings_account", "employ_since",
           "installment_rate", "sex_personal_status", "others_debits",
           "residence_since", "property", "age", "installment_plans",
           "housing", "n_credits_bank", "job", "n_people", "telephone",
           "foreign_worker", "StatLog"]

SET_ORDER = [["A14", "A11", "A12", "A13"],
             ["A34", "A33", "A32", "A31", "A30"],
             ["A410", "A43", "A47", "A44", "A45", "A41", "A40", "A42", "A48", "A46", "A49"],
             ["A65", "A61", "A62", "A63", "A64"],
             ["A71", "A72", "A73", "A74", "A75"],
             ["A101", "A102", "A103"],
             ["A124", "A123", "A122", "A121"],
             ["A141", "A142", "A143"],
             ["A151", "A153", "A152"],
             ["A171", "A172", "A173", "A174"],
             ["A191", "A192"],
             ["A201", "A202"],
             [1, 2]]


class DataPrep:
    def __init__(self):
        self.data = pd.read_csv("../data/german.data", sep=" ",
                                names=COLUMNS)

    def init_encoder(self):
        categorical = np.array([0, 2, 3, 5, 6, 8, 9, 11, 13, 14, 16, 18, 19])
        
        self.data.iloc[:, categorical] = self.data.iloc[:, categorical].apply(LabelEncoder().fit_transform)

    def main_encoder(self):
        to_order = np.array([0, 2, 3, 5, 6, 9, 11, 13, 14, 16, 18, 19, 20])

        encoder = OrdinalEncoder(categories=SET_ORDER)
        self.data.iloc[:, to_order] = encoder.fit_transform(self.data.iloc[:, to_order])
        
        self.data["personal_status"] = self.data["sex_personal_status"].copy()
        self.data.rename(columns={'sex_personal_status': 'sex'}, inplace=True)
        self.data.replace({'personal_status':{"A93": 0, "A95": 0, "A91":1, "A92": 2, "A94": 2}}, inplace=True)
        self.data.replace({'sex':{"A91": 0, "A93": 0, "A94": 0, "A92": 1, "A95": 1}}, inplace=True)

