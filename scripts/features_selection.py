# coding: utf-8

import argparse
import sys
import pickle
import numpy as np
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import SelectFromModel
from sklearn.svm import LinearSVC
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import LassoCV
from data_prep import DataPrep
from model import Model


class FeatureSelection:
    def __init__(self, df):
        # Extrai as caracteristicas de treinamento
        ml = Model(df, np.arange(df.shape[1] - 1), norm=True)
        self.x = ml.x
        self.y = ml.y

    def pvalue_selection(self, alpha):
        # Seleciona as caracteri9scas com pvalue <= alpha
        select = SelectKBest(k="all")
        select.fit(self.x, self.y)

        # Retorna os indices das caracteristicas selecionadas
        index = np.where(select.pvalues_ <= alpha)[0]
        return index

    def frommodel_selection(self, mfm, prt):
        if mfm == "linearsvc":
            clf = LinearSVC(C=prt, penalty="l1", dual=False).fit(self.x, self.y)

        if mfm == "extratrees":
            clf = ExtraTreesClassifier(n_estimators=int(prt))
            clf = clf.fit(self.x, self.y)

        if mfm == "lassocv":
            clf = LassoCV()
            clf = clf.fit(self.x, self.y)


        select  = SelectFromModel(clf, prefit=True)
        index = np.where(select.get_support())[0]
        return index

    def write_features(self, index, named):
        try:
            f = open("../data/features", "rb")
            features = pickle.load(f)
            f.close()

        except:
            features = {}

        features[named] = index

        f = open("../data/features", "wb")
        pickle.dump(features, f)
        f.close()


if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("-m", "--method", type=str,
                        help='metodo de selecão',
                        choices=['pvalue', 'frommodel'],
                        required=True)

    parser.add_argument("-p", "--prt", type=float,
                        help='paraemtro do algoritivo, C, alfa ou n_estimators',
                        default=0.05)

    parser.add_argument("-mfm", "--frommodel", type=str,
                        help='metodo para from model',
                        choices=['linearsvc', 'extratrees', 'lassocv'],
                        default='extratrees')


    args = parser.parse_args()


    # Leitura dos dados
    dp = DataPrep()

    # Codificação dos dados
    dp.main_encoder()
    df = dp.data

    fs = FeatureSelection(df)

    if args.method == "pvalue":
        idx = fs.pvalue_selection(args.prt)
        fs.write_features(idx, "%s%s" % (args.method, str(args.prt).replace(".", "")))

    if args.method == "frommodel":
        idx = fs.frommodel_selection(args.frommodel, args.prt)
        fs.write_features(idx, "%s_%s%s" % (args.method, args.frommodel,
                                            str(args.prt).replace(".", "")))

    print(idx)
