# coding: utf-8

import pickle
import argparse
import numpy as np
import pandas as pd
import seaborn as sn
from sklearn import metrics
import matplotlib.pyplot as plt

from data_prep import DataPrep
from model import Model


METHODS = ["random_forest", "mlp", "naive_bayes", "tree", "neighbors", "svm", "logistic"]


def run_test(method, encoder=0, normalize=0, feat=0):
    dp = DataPrep()

    if encoder:
        dp.main_encoder()

    else:
        dp.init_encoder()

    df = dp.data

    try:
        f = open("../data/features", "rb")
        features = pickle.load(f)
        f.close()
        features = features[feat]

    except:
        print("Erro na leitura das caracteristicas: Usando todas.")
        features = np.arange(df.shape[1] - 1)

    print("Features: ", len(features), df.axes[1][features])

    # Split dos dados e normalização
    ml = Model(df, features, norm=normalize)
    report = ml.test_model(method)
    return report


def setup_models():
    f = open("../data/features", "rb")
    features = pickle.load(f)
    f.close()

    MODELS = {"M0": {"encoder": 0, "normalize": 0, "feat": "all"},
              "M1": {"encoder": 1, "normalize": 0, "feat": "all"},
              "M2": {"encoder": 1, "normalize": 1, "feat": "all"}}
    i = 3
    for m in features.keys():
        MODELS["M%s" %i] = {"encoder": 1, "normalize": 1, "feat": m}
        i += 1

    f = open("../data/models", "wb")
    pickle.dump(MODELS, f)
    f.close()

    return MODELS


def write_report(MODELS):
    f = open("../data/report.txt", "w")
    f.write("method,model,target,precision,recall,score,accuracy\n")

    for method in METHODS:
        for model in MODELS.keys():
            try:
                report, acc = run_test(method, **MODELS[model])

                f.write("%s,%s,GOOD,%.2f,%.2f,%.2f,%.2f\n" % (method, model, report[0][0],
                                                              report[1][0], report[2][0],
                                                              acc))
                f.write("%s,%s,BAD,%.2f,%.2f,%.2f,%.2f\n" % (method, model, report[0][1],
                                                              report[1][1], report[2][1],
                                                              acc))
            except:
                print(model)
    
    f.close()


def plot(method, metric, path):
    df = pd.read_csv("../data/report.txt")

    df = df[df.method == method]
    plt.figure(figsize=(20,10))

    if metric == "accuracy":
        aux = df[df.target == "GOOD"]
        plt.plot(np.arange(len(aux)), aux[metric])
        init = aux[aux.model == "M0"][metric].values
        plt.plot([0, len(aux)], [init[0], init[0]], "k--")

    else:
        for label in ["GOOD", "BAD"]:
            aux = df[df.target == label]
            plt.plot(np.arange(len(aux)), aux[metric], label=label)
            init = aux[aux.model == "M0"][metric].values
            plt.plot([0, len(aux)], [init[0], init[0]], "k--")
        plt.legend(fontsize=12)

    plt.title(method, fontsize=14)
    plt.grid()
    plt.ylabel(metric)
    plt.xticks(np.arange(len(aux)), aux.model, fontsize=12)
    plt.savefig("%s%s_%s" %(path, metric, method))


if __name__ == "__main__":
    parser = argparse.ArgumentParser()


    parser.add_argument("-t", "--task", type=str,
                        help='report ou plot',
                        required=True)

    parser.add_argument("-m", "--method", type=str,
                        help='Metodo (plot)',
                        default="all")

    parser.add_argument("-mtc", "--metric", type=str,
                        help='Metrica (plot): recall, precision, score ou accuracy',
                        default="accuracy")

    parser.add_argument("-rm", "--read_models", type=int,
                        help='0 - criar models, 1 ler models (Report)',
                        default=0)

    parser.add_argument("-dir", "--fig_dir", type=str,
                        help='Diretório das figuras (plot)',
                        default="./")
                 
    args = parser.parse_args()

    if args.task == "report":
        if args.read_models:
            f = open("../data/models", "rb")
            models = pickle.load(f)
            f.close()
        else:
             models = setup_models()
        
        write_report(models)

    elif args.task == "plot":
        if args.method == "all":
            for method in METHODS:
                print(method)
                plot(method, args.metric, args.fig_dir)
        else:
            plot(args.method, args.metric, args.fig_dir)

    else:
        print("Use report or plot to -t")
    