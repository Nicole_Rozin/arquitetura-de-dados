# coding: utf-8

import argparse
import pickle
import numpy as np
import pandas as pd
import seaborn as sn
from sklearn import metrics
import matplotlib.pyplot as plt
from sklearn.naive_bayes import MultinomialNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from imblearn.over_sampling import SMOTE 
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.preprocessing import MinMaxScaler
from data_prep import DataPrep


methods = {"random_forest": RandomForestClassifier,
           "naive_bayes": MultinomialNB,
           "neighbors": KNeighborsClassifier,
           "tree": DecisionTreeClassifier,
           "mlp": MLPClassifier,
           "svm": SVC,
           "logistic": LogisticRegression}

# parameters = {"random_forest": {"n_estimators": (10, 20, 30, 40, 50, 100),
#                                 "criterion": ('gini', 'entropy'),
#                                 "max_depth": (5, 10, 15, 20, None),
#                                 "max_features": ('sqrt', 'auto', 'log2')},
#               "naive_bayes": {"alpha": (1e-3, 1e-2, 0.1, 0.5, 1.),
#                               "fit_prior": (True, False)},
#               "neighbors": {"n_neighbors": (2, 3, 5),
#                             "weights": ('uniform', 'distance'),
#                             "algorithm": ('auto', 'ball_tree',
#                                           'kd_tree', 'brute')},
#               "tree": {"splitter": ("best", "random"),
#                        "criterion": ('gini', 'entropy'),
#                        "max_depth": (5, 10, 15, 20, None),
#                        "max_features": ('sqrt', 'auto', 'log2'),
#                        "min_samples_split": (0.02, 0.05, 0.1, 0.15)},
#               "mlp": {"hidden_layer_sizes": (150, (20, 20, 20), (50, 50, 50),
#                                              (100, 100), (100, 100, 100)),
#                       "activation": ("identity", "logistic", "tanh", "relu"),
#                       "solver": ("lbfgs", "sgd", "adam"),
#                       "learning_rate": ("constant", "invscaling", "adaptive"),
#                       "learning_rate_init": (0.001, 0.005, 0.01),
#                       "max_iter": (200, 500, 1000),
#                       "tol": (0.0001, 0.001, 0.01, 0.05),
#                       "batch_size": (1, 2, 8, 16)},
#               "svm": {"C": (0.5, 1, 5, 10, 20, 50, 80, 100, 120),
#                       "kernel": ('linear', 'poly', 'rbf', 'sigmoid'),
#                       "degree": (2, 3, 4, 5)}}

parameters = {"random_forest": {},
              "naive_bayes": {},
              "neighbors": {},
              "tree": {},
              "mlp": {},
              "svm": {},
              "logistic":{"max_iter": [1000]}}


class Model:
    def __init__(self, data, features, norm=False, test_size=0.3,
                 to_pred="StatLog", cv=5, balance=0):
        # Divide o conjunto de dado em treinamento e teste
        self.x, self.x_test, self.y, self.y_test = self.split_data(data,
                                                                   to_pred,
                                                                   test_size)
        # Faz balanceamento
        if balance:
            sm = SMOTE(random_state=42)
            self.x, self.y = sm.fit_resample(self.x, self.y)

        # Normaliza
        if norm:
            scaler = MinMaxScaler()
            scaler.fit(self.x)

            self.x = scaler.transform(self.x)
            self.x_test = scaler.transform(self.x_test)

        # Pega as caracteristicas selecionadas
        self.x = self.x[:, features]
        self.x_test = self.x_test[:, features]
        print(self.x.shape)

        self.cv = cv

    def split_data(self, data, to_pred, testsize):
        # Seleciona a matriz X com todas as colunas exceto a ser prevista
        X = data.drop(columns=[to_pred]).values

        # Seleciona a coluna a ser prevista
        y = data[to_pred].values

        # Divide o conjunto mantendo a proporção das classes
        x_train, x_test, y_train, y_test = train_test_split(X, y,
                                                            test_size=testsize,
                                                            random_state=42,
                                                            stratify=y)
        return x_train, x_test, y_train, y_test

    def search_model(self, method):
        # Executa a busca em grade
        gs_clf = GridSearchCV(methods[method](), parameters[method],
                              cv=self.cv, n_jobs=-1)
        gs_clf.fit(self.x, self.y)

        # Salva os resultados e os melhores parametros
        result = {"best": gs_clf.best_params_, "results": gs_clf.cv_results_}

        f = open("../data/prts_%s" % (method), "wb")
        pickle.dump(result, f)
        f.close()

        print("Best parameters: ", result["best"])

    def test_model(self, method):
        # Lê o resultado da busca em grade
        f = open("../data/prts_%s" % (method), "rb")
        result = pickle.load(f)
        f.close()

        # Treina o modelo
        clf = methods[method](**result["best"])
        clf.fit(self.x, self.y)

        # Testa o modelo
        y_pred = clf.predict(self.x_test)

        print("Classification Report:")
        print(metrics.classification_report(self.y_test, y_pred))

        # Gera a imagem da matriz de confusão
        cm = metrics.confusion_matrix(self.y_test, y_pred)
        print("Classification Report:")
        print(cm)

        return (metrics.precision_recall_fscore_support(self.y_test, y_pred),
                metrics.accuracy_score(self.y_test, y_pred))


def plot_cm(cm, figname, labels):
    # Plota e salva a matriz de confusão
    df_cm = pd.DataFrame(cm.astype(int),
                         index=[i for i in labels],
                         columns=[i for i in labels])

    plt.figure(figsize=(6, 4))
    sn.heatmap(df_cm, annot=True, fmt="d")
    plt.ylabel("Observado", fontsize=12)
    plt.xlabel("Previsto", fontsize=12)
    plt.savefig(figname)
    plt.plot()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument("-m", "--method", type=str,
                        help='Classification method',
                        required=True)

    parser.add_argument("-test", "--test", type=int,
                        help='test model? 0 or 1',
                        default=0)

    parser.add_argument("-norm", "--normalize", type=int,
                        help='normalize? 0 or 1',
                        default=0)

    parser.add_argument("-cod", "--encoder", type=int,
                        help='Encoder? 0 or 1',
                        default=0)

    parser.add_argument("-feat", "--features", type=str,
                        help='Name in dict',
                        default="all")

    args = parser.parse_args()

    # Leitura dos dados e codificação
    dp = DataPrep()

    if args.encoder:
        dp.main_encoder()

    else:
        dp.init_encoder()

    df = dp.data

    try:
        f = open("../data/features", "rb")
        features = pickle.load(f)
        f.close()
        features = features[args.features]

    except:
        print("Usando todas as caracteristicas")
        features = np.arange(df.shape[1] - 1)

    print("Features: ", len(features), df.axes[1][features])

    # Split dos dados e normalização
    ml = Model(df, features, norm=args.normalize)

    if args.test:
        ml.test_model(args.method)
    else:
        ml.search_model(args.method)